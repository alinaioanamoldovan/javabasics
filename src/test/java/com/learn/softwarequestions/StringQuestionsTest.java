package com.learn.softwarequestions;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class StringQuestionsTest {

    private StringQuestions stringQuestions;

    @BeforeAll
    void init() {
        this.stringQuestions = new StringQuestions();
    }

    @Test
    void checkIfAnagrams() {
        assertThat("The response should be true", this.stringQuestions.checkIfAnagrams("mac", "cam"), is(true));
        assertThat("The response should be false for two string that are not anagrams", this.stringQuestions.checkIfAnagrams("mac", "sac"), is(false));
        assertThat("The response should be false for two strings that have different sizes", this.stringQuestions.checkIfAnagrams("mac", "stack"), is(false));
    }

    @Test
    void countOccurrences() {
        assertThat("The number of occurrences should be different from zero for a string that contains the specified char", this.stringQuestions.countOccurrences("mac", 'a'), not(0L));
    }

    @Test
    void reverseString() {
        final String reverseString = this.stringQuestions.reverseString("alina");
        assertThat("The reversed String should be equal to the expected one", reverseString, is("anila"));
        assertThat("The reversed String should not be equal to the original String if not palindrome", reverseString.equals("alina"), is(false));
    }

    @Test
    void checkIfPalindrome() {
        assertThat("The response is true for a palindrome string", this.stringQuestions.checkIfPalindrome("level"), is(true));
        assertThat("The response should be false for a string that is not a palindrome", this.stringQuestions.checkIfPalindrome("case"), is(false));
    }
}