package com.learn.softwarequestions;


import java.util.Arrays;

class StringQuestions {
    //How can we check if two Strings are anagrams in Java
    boolean checkIfAnagrams(final String s1, final String s2) {
        final int sizeString1 = s1.length();
        final int sizeString2 = s2.length();
        boolean areAnagrams = false;
        if (sizeString1 == sizeString2) {
            final char[] s1CharArray = s1.toCharArray();
            final char[] s2CharArray = s2.toCharArray();
            Arrays.sort(s1CharArray);
            Arrays.sort(s2CharArray);

            if (Arrays.equals(s1CharArray, s2CharArray)) {
                areAnagrams = true;
            }
        }

        return areAnagrams;
    }

    //How can we count the number of occurrences of a given character in a String?
    long countOccurrences(final String stringToCountFrom, final char forWhichToCountFor) {
        return stringToCountFrom.chars()
                                .filter(currentChar -> currentChar == forWhichToCountFor)
                                .count();
    }


    //How can we reverse a String in Java
    String reverseString(final String regularString) {
        return new StringBuilder(regularString).reverse().toString();
    }

    //How can we check if a String is a palindrome or not
    boolean checkIfPalindrome(final String stringToCheckFor) {
        boolean isPalindrome = false;
        final String reversedString = new StringBuilder(stringToCheckFor).reverse().toString();
        if (stringToCheckFor.equals(reversedString)) {
            isPalindrome = true;
        }
        return isPalindrome;
    }
}